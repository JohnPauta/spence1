// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyAQthCbWWXN4EWZSnURfULuF4fGn4sIU20",
  authDomain: "spance1.firebaseapp.com",
  projectId: "spance1",
  storageBucket: "spance1.appspot.com",
  messagingSenderId: "517170295928",
  appId: "1:517170295928:web:da7ee5d9d69f8b6489e98d",
  measurementId: "G-97FSTSPCMY"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
