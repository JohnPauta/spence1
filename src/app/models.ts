export interface InPaciente {
    uid: string;
    email: string;
    nombre: string;
    apellido: string;
    fecha: any;
}
