import { RespuestasPacienteComponent } from "./componentes/respuestas-paciente/respuestas-paciente.component";
import { CrearPacienteComponent } from "./componentes/crear-paciente/crear-paciente.component";
import { EditarPacienteComponent } from "./componentes/editar-paciente/editar-paciente.component";
import { NgModule } from "@angular/core";
import {
	PreloadAllModules,
	RouterModule,
	Routes,
	CanActivate,
} from "@angular/router";
import { ListaComponent } from "./componentes/lista/lista.component";
import { AuthGuard } from "./guards/auth.guard";
import { NologinGuard } from "./guards/nologin.guard";

const routes: Routes = [
	{
		path: "",
		redirectTo: "login",
		pathMatch: "full",
	},
	{
		path: "home",
		loadChildren: () =>
			import("./home/home.module").then((m) => m.HomePageModule),
		canActivate: [AuthGuard],
	},
	{
		path: "login",
		loadChildren: () =>
			import("./componentes/login/login.module").then((m) => m.LoginPageModule),
		canActivate: [NologinGuard],
	},
	{
		path: "test",
		loadChildren: () =>
			import("./paginas/test/test.module").then((m) => m.TestPageModule),
	},
	{
		path: "ver-listas",
		loadChildren: () =>
			import("./paginas/ver-listas/ver-listas.module").then(
				(m) => m.VerListasPageModule
			),
	},
	{
		path: "ver-listas",
		component: ListaComponent,
	},
	{
		path: "editar-paciente",
		component: EditarPacienteComponent,
	},
	{
		path: "crear-paciente",
		component: CrearPacienteComponent,
	},
	{
		path: "editarpaciente/:id",
		component: EditarPacienteComponent,
	},
	{
		path: "ver-preguntas-registradas",
		loadChildren: () =>
			import(
				"./paginas/ver-preguntas-registradas/ver-preguntas-registradas.module"
			).then((m) => m.VerPreguntasRegistradasPageModule),
	},
	{
		path: "ver-respuestas",
		loadChildren: () =>
			import("./paginas/ver-respuestas/ver-respuestas.module").then(
				(m) => m.VerRespuestasPageModule
			),
	},

	{
		path: "respuestas-paciente/:id",
		component: RespuestasPacienteComponent,
	},
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, relativeLinkResolution: 'legacy' }),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {}
