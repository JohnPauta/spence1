import { PacienteService } from "./../../servicios/paciente.service";
import { Component, OnInit } from "@angular/core";
import { ToastController } from "@ionic/angular";

@Component({
	selector: "app-lista",
	templateUrl: "./lista.component.html",
	styleUrls: ["./lista.component.scss"],
})
export class ListaComponent implements OnInit {
	paciente: any[] = [];

	constructor(
		private _pacienteService: PacienteService,
		private toastCtrl: ToastController
	) {}

	ngOnInit() {
		this.getPaciente();
	}

	async presentToast(message: string) {
		const toast = await this.toastCtrl.create({
			message,
			duration: 2000,
			color: "danger",
		});
		toast.present();
	}

	getPaciente() {
		this._pacienteService.getPacientes().subscribe((data) => {
			this.paciente = [];
			data.forEach((element: any) => {
				this.paciente.push({
					id: element.payload.doc.id,
					...element.payload.doc.data(),
				});
			});
			console.log(this.paciente);
		});
		
	}
	eliminarPaciente(id: string) {
		this._pacienteService
			.eliminarPaciente(id)
			.then(() => {
				this.presentToast("Paciente eliminado correctamente");
			})
			.catch((error) => {
				console.log(error);
			});
	}
}
