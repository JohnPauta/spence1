import { PreguntasPacienteComponent } from './preguntas-paciente/preguntas-paciente.component';
import { AuthService } from './../servicios/auth.service';
import { EditarPacienteComponent } from './editar-paciente/editar-paciente.component';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CrearPacienteComponent } from './crear-paciente/crear-paciente.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaComponent } from './lista/lista.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RespuestasPacienteComponent } from './respuestas-paciente/respuestas-paciente.component';

@NgModule({
  declarations: [ListaComponent, CrearPacienteComponent, EditarPacienteComponent, PreguntasPacienteComponent, RespuestasPacienteComponent],
  exports: [ListaComponent, CrearPacienteComponent, EditarPacienteComponent, PreguntasPacienteComponent, RespuestasPacienteComponent ],
  imports: [CommonModule, ReactiveFormsModule, IonicModule, RouterModule ],
})
export class ComponentesModule {}
