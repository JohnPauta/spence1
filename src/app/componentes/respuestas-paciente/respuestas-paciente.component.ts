import { PacienteService } from "./../../servicios/paciente.service";
import { ActivatedRoute } from "@angular/router";
import { RespuestasService } from "./../../servicios/respuestas.service";
import { Component, OnInit } from "@angular/core";

import * as html2pdf from "html2pdf.js";

//PRUEBA

// import { Platform } from "@ionic/angular";
// import { FileOpener } from "@ionic-native/file-opener/ngx";
// import { File } from "@ionic-native/file/ngx";
// import {
// 	FileTransfer
// } from "@ionic-native/file-transfer/ngx";

@Component({
	selector: "app-respuestas-paciente",
	templateUrl: "./respuestas-paciente.component.html",
	styleUrls: ["./respuestas-paciente.component.scss"],
})
export class RespuestasPacienteComponent implements OnInit {
	respuestas: any[] = [];
	id: string | null;
	nombre: any;
	apellido: any;
	fecha: any;
	pregunta: any[] = [];
	numero: any;
	puntaje0: any = "Nunca";
	puntaje1: any = "A veces";
	puntaje2: any = "Muchas veces";
	puntaje3: any = "Siempre";
	puntajesi: any;
	trastorno1: number = 0;
	trastorno2: number = 0;
	trastorno3: number = 0;
	trastorno4: number = 0;
	trastorno5: number = 0;
	trastorno6: number = 0;
	storageDirectory: any;

	downloadPDF() {
		const options = {
			name: "output.pdf",
			image: { type: "jpeg" },
			html2canvas: {},
			jsPDF: { orientation: "portrait" },
		};
		const element: Element = document.getElementById("PDF");

		html2pdf().from(element).set(options).save();
	}

	getPuntaje() {
		const preguntaf1 = [
			this.respuestas[13 - 1].respuesta,
			this.respuestas[21 - 1].respuesta,
			this.respuestas[28 - 1].respuesta,
			this.respuestas[30 - 1].respuesta,
			this.respuestas[32 - 1].respuesta,
			this.respuestas[34 - 1].respuesta,
			this.respuestas[36 - 1].respuesta,
			this.respuestas[37 - 1].respuesta,
			this.respuestas[39 - 1].respuesta,
		];

		const preguntaf2 = [
			this.respuestas[5 - 1].respuesta,
			this.respuestas[8 - 1].respuesta,
			this.respuestas[12 - 1].respuesta,
			this.respuestas[15 - 1].respuesta,
			this.respuestas[16 - 1].respuesta,
			this.respuestas[44 - 1].respuesta,
		];

		const preguntaf3 = [
			this.respuestas[6 - 1].respuesta,
			this.respuestas[7 - 1].respuesta,
			this.respuestas[9 - 1].respuesta,
			this.respuestas[10 - 1].respuesta,
			this.respuestas[29 - 1].respuesta,
			this.respuestas[35 - 1].respuesta,
		];

		const preguntaf4 = [
			this.respuestas[2 - 1].respuesta,
			this.respuestas[18 - 1].respuesta,
			this.respuestas[23 - 1].respuesta,
			this.respuestas[25 - 1].respuesta,
			this.respuestas[33 - 1].respuesta,
		];

		const preguntaf5 = [
			this.respuestas[14 - 1].respuesta,
			this.respuestas[19 - 1].respuesta,
			this.respuestas[27 - 1].respuesta,
			this.respuestas[40 - 1].respuesta,
			this.respuestas[41 - 1].respuesta,
			this.respuestas[42 - 1].respuesta,
		];

		const preguntaf6 = [
			this.respuestas[1 - 1].respuesta,
			this.respuestas[3 - 1].respuesta,
			this.respuestas[4 - 1].respuesta,
			this.respuestas[20 - 1].respuesta,
			this.respuestas[22 - 1].respuesta,
			this.respuestas[24 - 1].respuesta,
		];

		this.trastorno1 = this.sumaTotal(preguntaf1);
		this.trastorno2 = this.sumaTotal(preguntaf2);
		this.trastorno3 = this.sumaTotal(preguntaf3);
		this.trastorno4 = this.sumaTotal(preguntaf4);
		this.trastorno5 = this.sumaTotal(preguntaf5);
		this.trastorno6 = this.sumaTotal(preguntaf6);

		console.log("Puntaje trastorno 1: " + this.trastorno1);
		console.log("Puntaje trastorno 2: " + this.trastorno2);
		console.log("Puntaje trastorno 3: " + this.trastorno3);
		console.log("Puntaje trastorno 4: " + this.trastorno4);
		console.log("Puntaje trastorno 5: " + this.trastorno5);
		console.log("Puntaje trastorno 6: " + this.trastorno6);
	}

	sumaTotal(respuestas) {
		let acumulador = 0;

		for (let index = 0; index < respuestas.length; index++) {
			const respuesta = respuestas[index];
			// console.log(respuesta)
			acumulador = acumulador + this.getTransformar(respuesta);
		}
		return acumulador;
		// console.log(acumulador);
	}

	getTransformar(valor) {
		const objeto1 = {
			Nunca: 0,
			"A veces": 1,
			"Muchas veces": 2,
			Siempre: 3,
		};
		// console.log(objeto1[valor])
		return objeto1[valor];
	}

	constructor(
		private _respuestaService: RespuestasService,
		private _pacienteService: PacienteService,
		private aRoute: ActivatedRoute,
		//Prueba
		// private transfer: FileTransfer,
		// private file12: File,
		// private platform: Platform,
		// private fileOpener: FileOpener
	) //PRUEBA

	{
		this.id = this.aRoute.snapshot.paramMap.get("id");
		console.log("this.id", this.id);
		
		//Prueba
		// this.platform.ready().then(() => {
		// 	if (!this.platform.is("cordova")) {
		// 		return false;
		// 	}

		// 	if (this.platform.is("ios")) {
		// 		this.storageDirectory = this.file12.externalDataDirectory;
		// 	} else if (this.platform.is("android")) {
		// 		this.storageDirectory = this.file12.externalDataDirectory;
		// 	} else {
		// 		return false;
		// 	}
		// });
	}

	ngOnInit() {
		this.getRespuestas();
	}

	getRespuestas() {
		this._pacienteService
			.getDocumento("paciente", this.id)
			.subscribe((res: any) => {
				console.log(res);
				this.respuestas = res.respuestas;
				this.nombre = res.nombre;
				this.apellido = res.apellido;
				this.fecha = res.fecha.seconds;
				this.pregunta = res.pregunta;
				this.numero = res.numero;

				this.getPuntaje();
			});
		return;
	}

	esEditar() {
		if (this.id !== null) {
			this._respuestaService.getRespuestad(this.id).subscribe((data) => {
				console.log(data.payload.data()["nombre"]);
			});
		}
	}
}
