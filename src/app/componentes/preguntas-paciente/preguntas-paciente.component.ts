import { PacienteService } from "./../../servicios/paciente.service";
import { Component, OnInit } from "@angular/core";

@Component({
	selector: "app-preguntas-paciente",
	templateUrl: "./preguntas-paciente.component.html",
	styleUrls: ["./preguntas-paciente.component.scss"],
})
export class PreguntasPacienteComponent implements OnInit {
	paciente: any[] = [];

	constructor(private _pacienteService: PacienteService) {}

	ngOnInit() {
		this.getPaciente();
	}

	getPaciente() {
		this._pacienteService.getPacientes().subscribe((data) => {
			this.paciente = [];
			data.forEach((element: any) => {
				this.paciente.push({
					id: element.payload.doc.id,
					...element.payload.doc.data(),
				});
			});
			console.log(this.paciente);
		});
	}
}
