import { Router } from "@angular/router";
import { AuthService } from "./../../servicios/auth.service";
import { PacienteService } from "./../../servicios/paciente.service";
import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { InPaciente } from "src/app/models";

@Component({
  selector: "app-crear-paciente",
  templateUrl: "./crear-paciente.component.html",
  styleUrls: ["./crear-paciente.component.scss"],
})
export class CrearPacienteComponent implements OnInit {
  createPaciente: FormGroup;
  submitted = false;

  @Output() next = new EventEmitter<void>();

  inpaciente: InPaciente = {
    uid: "",
    email: "",
    nombre: "",
    apellido: "",
    fecha: ""
  };

  constructor(
    private fb: FormBuilder,
    private _pacienteService: PacienteService,
    private router: Router,
    private AuthService: AuthService
  ) {
    this.createPaciente = this.fb.group({
      nombre: ["", Validators.required],
      apellido: ["", Validators.required],
    });
  }

  ngOnInit() {}

  swipeNext() {
    if (this.createPaciente.invalid) {
      return;
    } else {
      this.next.emit();
    }
    const credenciales = {
      email: this.inpaciente.nombre + this.inpaciente.apellido + "@gmail.com",
      password: "123456789",
      nombre: this.inpaciente.nombre,
      apellido: this.inpaciente.apellido,
    };

    const res = this.AuthService.register(
      credenciales.email,
      credenciales.password,
      this.inpaciente.nombre,
      this.inpaciente.apellido,
      this.inpaciente.fecha = new Date()

    ).catch((err) => {
      console.log("error =>", res);
    });
    console.log(res);
  }

  agregarPaciente() {
    this.submitted = true;

    if (this.createPaciente.invalid) {
      return;
    }
    const paciente: any = {
      nombre: this.createPaciente.value.nombre,
      apellido: this.createPaciente.value.apellido,
      fecha: new Date(),
    };
    // this._pacienteService
    //   .agregarPaciente(paciente)
    //   .then(() => {
    //     console.log("Paciente registrado con éxito!");
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });
  }
}
