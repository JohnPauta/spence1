import { ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { PacienteService } from './../../servicios/paciente.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-editar-paciente',
  templateUrl: './editar-paciente.component.html',
  styleUrls: ['./editar-paciente.component.scss'],
})
export class EditarPacienteComponent implements OnInit {
  createPaciente: FormGroup;
  submitted = false;
  loading = false;
  id: string | null;

  @Output() next = new EventEmitter<void>();

  constructor(
    private fb: FormBuilder,
    private _pacienteService: PacienteService,
    private aRoute: ActivatedRoute,
    private toastCtrl: ToastController
  ) {
    this.createPaciente = this.fb.group({
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
    });
    this.id = this.aRoute.snapshot.paramMap.get('id');
    console.log(this.id);
  }

  ngOnInit(): void {
    this.editPaciente();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({
      message,
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  agregarEditarPaciente() {
    this.submitted = true;

    if (this.createPaciente.invalid) {
      return;
    }
    if (this.id === null) {
      this.agregarPaciente();
    } else {
      this.editarPaciente();
    }
  }

  agregarPaciente() {
    const paciente: any = {
      nombre: this.createPaciente.value.nombre,
      apellido: this.createPaciente.value.apellido,
      fechaCreacion: new Date(),
      fechaActualizacion: new Date(),
    };
    this.loading = true;
    this._pacienteService
      .agregarPaciente(paciente)
      .then(() => {
        console.log('Paciente registrado con éxito!');
        this.loading = false;
      })
      .catch((error) => {
        console.log(error);
        this.loading = false;
      });
  }

  editarPaciente() {
    const paciente: any = {
      nombre: this.createPaciente.value.nombre,
      apellido: this.createPaciente.value.apellido,
      fechaActualizacion: new Date(),
    };
    this._pacienteService.actualizarPaciente(this.id, paciente).then(() => {
      this.presentToast('Datos Actualizados Correctamente');
    });
  }

  editPaciente() {
    if (this.id !== null) {
      this._pacienteService.getPaciente(this.id).subscribe((data) => {
        console.log(data.payload.data().nombre);
        this.createPaciente.setValue({
          nombre: data.payload.data()['nombre'],
          apellido: data.payload.data()['apellido'],
        });
      });
    }
  }
}
