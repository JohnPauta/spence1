import { LoadingController } from "@ionic/angular";
import { Component, OnInit } from "@angular/core";

@Component({
	selector: "app-ver-listas",
	templateUrl: "./ver-listas.page.html",
	styleUrls: ["./ver-listas.page.scss"],
})
export class VerListasPage implements OnInit {
	loading: HTMLIonLoadingElement;

	constructor(private loadingController: LoadingController) {}

	ngOnInit() {
		setTimeout(() => {
			this.loading.dismiss();
		}, 1500);

		this.presentLoading("Cargando");
	}

	async presentLoading(message: string) {
		this.loading = await this.loadingController.create({
			cssClass: "my-custom-class",
			message,
		});
		return this.loading.present();
	}
}
