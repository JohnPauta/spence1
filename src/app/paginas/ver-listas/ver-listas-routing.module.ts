import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VerListasPage } from './ver-listas.page';

const routes: Routes = [
  {
    path: '',
    component: VerListasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VerListasPageRoutingModule {}
