import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { VerListasPageRoutingModule } from "./ver-listas-routing.module";

import { VerListasPage } from "./ver-listas.page";
import { ComponentesModule } from "src/app/componentes/componentes.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		VerListasPageRoutingModule,
		ComponentesModule,
		ReactiveFormsModule,
	],
	declarations: [VerListasPage],
})
export class VerListasPageModule {}
