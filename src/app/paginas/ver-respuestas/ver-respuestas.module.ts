import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { VerRespuestasPageRoutingModule } from "./ver-respuestas-routing.module";

import { VerRespuestasPage } from "./ver-respuestas.page";
import { ComponentesModule } from "src/app/componentes/componentes.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		VerRespuestasPageRoutingModule,
		ComponentesModule,
		ReactiveFormsModule
	],
	declarations: [VerRespuestasPage],
})
export class VerRespuestasPageModule {}
