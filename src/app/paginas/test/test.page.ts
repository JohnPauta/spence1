import { AuthService } from "./../../servicios/auth.service";
import { respuestae } from "./../../models/respuesta";
import { PacienteService } from "./../../servicios/paciente.service";
import { Observable } from "rxjs";

import {
	AngularFirestore,
	AngularFirestoreCollection,
	AngularFirestoreDocument,
} from "@angular/fire/firestore";
import { Component, OnInit, ViewChild } from "@angular/core";
import { IonSlides } from "@ionic/angular";

@Component({
	selector: "app-test",
	templateUrl: "./test.page.html",
	styleUrls: ["./test.page.scss"],
})
export class TestPage implements OnInit {
	constructor(
		private _pacienteService: PacienteService,
		private authService: AuthService
	) {}
	value = 0;

	respuestas: Respuesta[] = [];
	preguntastex: any;
	public respuestae: respuestae;
	pacientesCol: AngularFirestoreCollection;
	uid: string;

	slideOpts = {
		allowTouchMove: false,
	};

	@ViewChild("mySlider") slides: IonSlides;

	slides1: { img: string; titulo: string; desc: string }[] = [
		{
			img: "/assets/icon/hola.svg",
			titulo: "Bienvenido",
			desc:
				"Recuerda tomarte tu tiempo para realizar cada pregunta, no hay razon por apresurarse y diviertete",
		},
	];

	preguntas: { img: string; pregunta: string }[] = [
		{
			img: "/assets/icon/nervioso.svg",
			pregunta: "Hay cosas que me preocupan",
		},

		{
			img: "/assets/icon/fantasma.svg",
			pregunta: "Me da miedo la oscuridad",
		},

		{
			img: "/assets/icon/dolor-de-estomago.svg",
			pregunta:
				"Cuando tengo un problema noto una sensación extraña en el estomago",
		},

		{
			img: "/assets/icon/temor.svg",
			pregunta: "Tengo miedo",
		},

		{
			img: "/assets/icon/casa.svg",
			pregunta: "Tendría miedo si me quedara solo en casa",
		},

		{
			img: "/assets/icon/prueba.svg",
			pregunta: "Me da miedo hacer un examen",
		},

		{
			img: "/assets/icon/bano-publico.svg",
			pregunta: "Me da miedo usar aseos público",
		},

		{
			img: "/assets/icon/padres.svg",
			pregunta: "Me preocupo cuando estoy lejos de mis padres",
		},

		{
			img: "/assets/icon/espectadores.svg",
			pregunta: "Tengo miedo de hacer el ridículo delante de la gente",
		},

		{
			img: "/assets/icon/libro.svg",
			pregunta: "Me preocupa hacer mal el trabajo de la escuela",
		},

		{
			img: "/assets/icon/chico.svg",
			pregunta: "Soy popular entre los niños y niñas de mi edad",
		},

		{
			img: "/assets/icon/paciente.svg",
			pregunta: "Me preocupa que algo malo le suceda a alguien de mi familia",
		},

		{
			img: "/assets/icon/respiracion-dificultosa.svg",
			pregunta: "De repente siento que no puedo respirar sin motivo",
		},

		{
			img: "/assets/icon/observacion.svg",
			pregunta:
				"Necesito comprobar varias veces que he hecho bien las cosas (como apagar la luz)",
		},

		{
			img: "/assets/icon/pesadilla.svg",
			pregunta: "Me da miedo dormir solo",
		},

		{
			img: "/assets/icon/caminar.svg",
			pregunta:
				"Estoy nervioso o tengo miedo por las mañanas antes de ir al colegio",
		},

		{
			img: "/assets/icon/primer-lugar.svg",
			pregunta: "Soy bueno en los deportes",
		},

		{
			img: "/assets/icon/mascota.svg",
			pregunta: "Me dan miedo los perros",
		},

		{
			img: "/assets/icon/pensamiento-negativo.svg",
			pregunta: "No puedo dejar de pensar en cosas malas o tontas",
		},

		{
			img: "/assets/icon/dolor-o-presion-en-el-pecho.svg",
			pregunta: "Cuando tengo un problema mi corazón late muy fuerte",
		},

		{
			img: "/assets/icon/temblor.svg",
			pregunta: "De repente empiezo a temblar sin motivo",
		},

		{
			img: "/assets/icon/ansiedad.svg",
			pregunta: "Me preocupa que algo malo pueda pasarme",
		},

		{
			img: "/assets/icon/dentista.svg",
			pregunta: "Me da miedo ir al médico o al dentista",
		},

		{
			img: "/assets/icon/problema.svg",
			pregunta: "Cuando tengo un problema me siento nervioso",
		},

		{
			img: "/assets/icon/mareado.svg",
			pregunta: "Me dan miedo los lugares altos o los ascensores",
		},

		{
			img: "/assets/icon/buena-persona.svg",
			pregunta: "Soy una buena persona",
		},

		{
			img: "/assets/icon/pensando.svg",
			pregunta:
				"Tengo que pensar en cosas especiales (un número o en una palabra) para evitar que pase algo malo",
		},

		{
			img: "/assets/icon/viajes.svg",
			pregunta: "Me da miedo viajar en coche, autobús o tren",
		},

		{
			img: "/assets/icon/auriculoterapia.svg",
			pregunta: "Me preocupa lo que otras personas piensan de mí",
		},

		{
			img: "/assets/icon/multitud.svg",
			pregunta:
				"Me da miedo estar en lugares donde hay mucha gente (centros comerciales, cines, autobuses, parques)",
		},
		{
			img: "/assets/icon/smiley.svg",
			pregunta: "Me siento feliz",
		},

		{
			img: "/assets/icon/sin_sentido.svg",
			pregunta: "De repente tengo mucho miedo sin motivo",
		},

		{
			img: "/assets/icon/insectos.svg",
			pregunta: "Me dan miedo los insectos o las arañas",
		},

		{
			img: "/assets/icon/desmayarse.svg",
			pregunta:
				"De repente me siento mareado o creo que me voy a desmayar sin motivo",
		},

		{
			img: "/assets/icon/habla.svg",
			pregunta:
				"Me da miedo tener que hablar delante de mis compañeros de clase",
		},

		{
			img: "/assets/icon/corazon.svg",
			pregunta: "De repente mi corazón late muy rápido sin motivo",
		},

		{
			img: "/assets/icon/preocupado.svg",
			pregunta:
				"Me preocupa tener miedo de repente sin que haya nada que temer",
		},

		{
			img: "/assets/icon/espejo.svg",
			pregunta: "Me gusta como soy",
		},

		{
			img: "/assets/icon/cerrado.svg",
			pregunta:
				"Me da miedo estar en lugares pequeños y cerrados (como túneles o habitaciones pequeñas)",
		},

		{
			img: "/assets/icon/gestion-del-tiempo.svg",
			pregunta:
				"Tengo que hacer algunas cosas una y otra vez (lavarme las manos, limpiar, o poner cosas en un orden determinado)",
		},

		{
			img: "/assets/icon/molesto.svg",
			pregunta:
				"Me molestan pensamientos tontos o malos, o imágenes en mi mente",
		},

		{
			img: "/assets/icon/no-tocar.svg",
			pregunta:
				"Tengo que hacer algunas cosas de una forma determinada para evitar que pasen cosas malas",
		},

		{
			img: "/assets/icon/leyendo.svg",
			pregunta: "Me siento orgulloso de mi trabajo en la escuela",
		},

		{
			img: "/assets/icon/pueblo.svg",
			pregunta: "Me daría miedo pasar la noche lejos de mi casa",
		},
	];

	final: { img: string; titulo: string }[] = [
		{
			img: "/assets/icon/terminar.svg",
			titulo:
				"Listo! Has finalizado el Test. Muchas gracias por tu Participación",
		},
	];

	swipeNext(index: number) {
		this.slides.slideNext();
		if (index == 0 || index > 0) {
			const respuesta: Respuesta = {
				numero: index + 1,
				respuesta: this.value,
				pregunta: this.preguntas[index],
			};
			this.respuestas[index] = respuesta;
			console.log(this.respuestas);
			const path = "paciente/";
			const data: Encuesta = {
				respuestas: this.respuestas,
			};
			const prueba = this.preguntastex;
			const id = this.uid;
			console.log(this.uid);
			this._pacienteService.setDoc(path, data, id, prueba).then(() => {
				console.log("Respuesta guardada");
			});
		}
		this.value = 0;
	}

	Onlogout() {
		this.authService.logout();
	}

	swipePrev() {
		this.slides.slidePrev();
	}

	saveEncuesta() {
		const path = "paciente/";
	}

	ngOnInit() {
		this.authService.getState().subscribe((res) => {
			if (res) {
				this.uid = res.uid;
			}
			console.log(res);
		});
	}
}

interface Respuesta {
	numero: number;
	respuesta: number;
	pregunta: any;
}

interface Encuesta {
	respuestas: Respuesta[];
}
