import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModificarRegistroPageRoutingModule } from './modificar-registro-routing.module';

import { ModificarRegistroPage } from './modificar-registro.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModificarRegistroPageRoutingModule
  ],
  declarations: [ModificarRegistroPage]
})
export class ModificarRegistroPageModule {}
