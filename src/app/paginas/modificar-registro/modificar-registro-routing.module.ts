import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModificarRegistroPage } from './modificar-registro.page';

const routes: Routes = [
  {
    path: '',
    component: ModificarRegistroPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModificarRegistroPageRoutingModule {}
