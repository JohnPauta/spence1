import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VerPreguntasRegistradasPage } from './ver-preguntas-registradas.page';

const routes: Routes = [
  {
    path: '',
    component: VerPreguntasRegistradasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VerPreguntasRegistradasPageRoutingModule {}
