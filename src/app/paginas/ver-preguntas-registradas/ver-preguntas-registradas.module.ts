import { ComponentesModule } from "./../../componentes/componentes.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { VerPreguntasRegistradasPageRoutingModule } from "./ver-preguntas-registradas-routing.module";

import { VerPreguntasRegistradasPage } from "./ver-preguntas-registradas.page";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		VerPreguntasRegistradasPageRoutingModule,
		ComponentesModule,
		ReactiveFormsModule,
	],
	declarations: [VerPreguntasRegistradasPage],
})
export class VerPreguntasRegistradasPageModule {}
