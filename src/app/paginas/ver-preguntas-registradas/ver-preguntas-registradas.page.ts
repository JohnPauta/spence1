import { Component, OnInit } from "@angular/core";
import { LoadingController } from "@ionic/angular";

@Component({
	selector: "app-ver-preguntas-registradas",
	templateUrl: "./ver-preguntas-registradas.page.html",
	styleUrls: ["./ver-preguntas-registradas.page.scss"],
})
export class VerPreguntasRegistradasPage implements OnInit {
	loading: HTMLIonLoadingElement;

	constructor(private loadingController: LoadingController) {}

	ngOnInit() {
		setTimeout(() => {
			this.loading.dismiss();
		}, 1500);

		this.presentLoading("Cargando");
	}
	async presentLoading(message: string) {
		this.loading = await this.loadingController.create({
			cssClass: "my-custom-class",
			message,
		});
		return this.loading.present();
	}
}
