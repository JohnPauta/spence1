import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VerPreguntasRegistradasPage } from './ver-preguntas-registradas.page';

describe('VerPreguntasRegistradasPage', () => {
  let component: VerPreguntasRegistradasPage;
  let fixture: ComponentFixture<VerPreguntasRegistradasPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ VerPreguntasRegistradasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VerPreguntasRegistradasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
