import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class PreguntasService {
  constructor(public storage: Storage) {}

  getQuestionData() {
    return this.storage.get("questions");
  }
}
