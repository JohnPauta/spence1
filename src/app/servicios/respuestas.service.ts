import { Observable } from "rxjs";
import { AngularFirestore } from "@angular/fire/firestore";
import { Injectable } from "@angular/core";

@Injectable({
	providedIn: "root",
})
export class RespuestasService {
	constructor(private firestore: AngularFirestore) {}

	getRespuestas(): Observable<any> {
		return this.firestore.collection("paciente").snapshotChanges();
	}

	getRespuestad(id: string): Observable<any> {
		return this.firestore.collection("paciente").doc(id).snapshotChanges();
	}
}
