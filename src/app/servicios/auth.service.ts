import { AngularFirestore } from "@angular/fire/firestore";

import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from "@angular/router";

@Injectable({
	providedIn: "root",
})
export class AuthService {
	constructor(
		private AFauth: AngularFireAuth,
		private Router: Router,
		private db: AngularFirestore
	) {}

	login(email: string, password: string) {
		return new Promise((resolve, rejected) => {
			this.AFauth.signInWithEmailAndPassword(email, password)
				.then((user) => {
					resolve(user);
				})
				.catch((err) => rejected(err));
		});
	}

	logout() {
		this.AFauth.signOut().then(() => {
			this.Router.navigate(["/login"]);
		});
	}

	register(
		email: string,
		password: string,
		nombre: string,
		apellido: string,
		fecha: any
	) {
		return new Promise((resolve, reject) => {
			this.AFauth.createUserWithEmailAndPassword(email, password)
				.then((res) => {
					console.log(res.user.uid);
					const uid = res.user.uid;
					resolve(res);
					this.db.collection("paciente").doc(uid).set({
						email: email,
						uid: uid,
						nombre: nombre,
						apellido: apellido,
						fecha: fecha,
					});
				})
				.catch((err) => reject(err));
		});
	}

	getState() {
		return this.AFauth.authState;
	}
}
