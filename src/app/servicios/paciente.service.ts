import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { promise } from "selenium-webdriver";

@Injectable({
	providedIn: "root",
})
export class PacienteService {
	constructor(private firestore: AngularFirestore) {}

	getCollection(path: string) {
		return this.firestore.collection(path).valueChanges();
	}

	getDocumento(path: string, id: string) {
		return this.firestore.collection(path).doc(id).valueChanges();
	}

	agregarPaciente(paciente: any): Promise<any> {
		return this.firestore.collection("paciente").add(paciente);
	}
	getPacientes(): Observable<any> {
		return this.firestore.collection("paciente").snapshotChanges();
	}
	eliminarPaciente(id: string): Promise<any> {
		return this.firestore.collection("paciente").doc(id).delete();
	}
	getPaciente(id: string): Observable<any> {
		return this.firestore.collection("paciente").doc(id).snapshotChanges();
	}
	actualizarPaciente(id: string, data: any): Promise<any> {
		return this.firestore.collection("paciente").doc(id).update(data);
	}
	agregarRespuesta(respuesta: any): Promise<any> {
		return this.firestore.collection("respuesta").add(respuesta);
	}

	setDoc(path: string, data: any, id: string, prueba:any) {
		return this.firestore.collection(path).doc(id).update(data);
	}
}
